package com.healthcare.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.healthcare.model.User;
import com.healthcare.service.InsauranceService;

@Controller
@RequestMapping("/")
public class AppController {
 
	@Autowired
	InsauranceService insauranceService;
	
     
    @Autowired
    MessageSource messageSource;
 
  
    
    @RequestMapping(value = { "/","/insurance" }, method = RequestMethod.GET)
    public String insaurance(ModelMap model) {
          User user=new User();
//        List<Employee> employees = service.findAllEmployees();
        model.addAttribute("user", user);
        return "insaurance";
    }
    
    @RequestMapping(value = { "/InsuranceRequest" }, method = RequestMethod.POST)
    public String InsauranceResult(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
         
    	String name = request.getParameter("name");
	    String ageString = request.getParameter("age");
	    String gender = request.getParameter("gender");
	    boolean hypertension = request.getParameter("hypertension") != null;
	    boolean bPressure = request.getParameter("bloodPressure") != null;
	    boolean bSugar = request.getParameter("bloodSugar") != null;
	    boolean overweight = request.getParameter("overweight") != null;
	    boolean smoking = request.getParameter("smoking") != null;
	    boolean alcohol = request.getParameter("alcohol") != null;
	    boolean exercise = request.getParameter("exercise") != null;
	    boolean drugs = request.getParameter("drugs") != null;
	    
	    int age=Integer.parseInt(ageString);
	    double premium= insauranceService.calculatePremium(gender, age, hypertension, bPressure, bSugar, overweight, smoking, alcohol, exercise, drugs);
	    
	    request.setAttribute("name", name);
	    request.setAttribute("premium", premium);
    	
        return "result";
    }
    
    
    
    
    
}