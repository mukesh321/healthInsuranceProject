package com.healthcare.model;


import java.util.List;

public class User {
	
	private String name;
	
	private int age;
	
	private String gender;
	
	private List<String> habits;
	
	private List<String> currentHealth;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<String> getHabits() {
		return habits;
	}

	public void setHabits(List<String> habits) {
		this.habits = habits;
	}

	public List<String> getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(List<String> currentHealth) {
		this.currentHealth = currentHealth;
	}

	
	
	
	

}