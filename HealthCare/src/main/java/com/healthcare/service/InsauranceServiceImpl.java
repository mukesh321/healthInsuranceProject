package com.healthcare.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("insauranceService")
@Transactional
public class InsauranceServiceImpl implements InsauranceService {
	
	
	
		public  double calculatePremium(String gender, int age, boolean hypertension, boolean bPressure,
				boolean bSugar, boolean overweight, boolean smoking, boolean alcohol, boolean exercise, boolean drugs) {
			double premium = 5000;
			
			if (age >= 18) {

				double val = age / 5.0;

				if (val < 8) {
					if (val < 5) {
						val = 1;
					} else {
						val = val - 4;
						if (val > 0.0) {
							val++;
						}
					}

					premium = premium + premium * (int) val * 0.10;
				} else {
					premium = premium + premium * 3 * 0.10;
					val = val - 8;
					if (val > 0.0) {
						val++;
					}
					premium = premium + premium * (int) val * 0.20;
				}

			}

			if (gender.equalsIgnoreCase("M")) {
				premium = premium + premium * 0.02;
			}
			if (hypertension == true) {
				premium = premium + premium * 0.01;
			}
			if (bPressure == true) {
				premium = premium + premium * 0.01;
			}
			if (bSugar == true) {
				premium = premium + premium * 0.01;
			}

			if (overweight == true) {
				premium = premium + premium * 0.01;
			}

			if (exercise == true) {
				premium = premium - premium * 0.03;
			}

			if (smoking == true) {
				premium = premium + premium * 0.03;
			}

			if (alcohol == true) {
				premium = premium + premium * 0.03;
			}

			if (drugs == true) {
				premium = premium + premium * 0.03;
			}

			return premium;

		}
	
	
	

}
